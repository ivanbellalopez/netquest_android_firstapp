package com.ivanbellalopez.firstapp;

import android.os.Handler;
import android.os.Bundle;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DisplayClockActivity extends AppCompatActivity {

    private final static int FIRST_APP_UPDATE_TIMER_SECS = 10;
    private final static String FIRST_APP_TIME_FORMAT = "dd/MM/yyyy HH:mm:ss.SSS";
    private final static SimpleDateFormat dateFormat = new SimpleDateFormat(FIRST_APP_TIME_FORMAT, Locale.getDefault());
    private final static Handler timeHandler = new Handler(Looper.getMainLooper());

    @Bind(R.id.welcomeLabel) TextView welcomeLabel;
    @Bind(R.id.timeDisplayLabel) TextView timeLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_clock);
        ButterKnife.bind(this);

        String message = getIntent().getStringExtra(MainActivity.FIRST_APP_NAME);
        welcomeLabel.setText(getString(R.string.label_hello, message));
    }

    @Override
    protected void onResume() {
        super.onResume();
        timeHandler.postDelayed(updateTimeRunnable, FIRST_APP_UPDATE_TIMER_SECS);
    }

    @Override
    protected void onPause() {
        super.onPause();
        timeHandler.removeCallbacksAndMessages(null);
    }

    private final Runnable updateTimeRunnable = new Runnable() {
        @Override
        public void run() {
            timeLabel.setText(dateFormat.format(Calendar.getInstance().getTime()));
            timeHandler.postDelayed(this, FIRST_APP_UPDATE_TIMER_SECS);
        }
    };
}