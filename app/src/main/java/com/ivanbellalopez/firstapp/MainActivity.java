package com.ivanbellalopez.firstapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    public final static String FIRST_APP_NAME = "com.mycompany.myfirstapp.NAME";
    private final static int FIRST_APP_TOAST_DELAY = 2000;

    @Bind(R.id.nameField) EditText nameField;
    @Bind(R.id.submitButton)  Button submitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);

        if (nameField != null) {
            imm.hideSoftInputFromWindow(nameField.getWindowToken(), 0);
        }

        return true;
    }

    @OnClick(R.id.submitButton)
    public void submitButton(View view) {
        if (TextUtils.isEmpty(nameField.getText())) {
            showErrorMessage();
        } else {
            goToNextScreen();
        }
    }

    private void showErrorMessage() {
        submitButton.setEnabled(false);

        Toast.makeText(this, getApplication().getString(R.string.label_error_empty_text), Toast.LENGTH_SHORT).show();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                submitButton.setEnabled(true);
            }
        }, FIRST_APP_TOAST_DELAY);
    }

    private void goToNextScreen () {
        Intent intent = new Intent(this, DisplayClockActivity.class);
        String message = nameField.getText().toString();
        intent.putExtra(FIRST_APP_NAME, message);
        startActivity(intent);
    }
}